# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: aimeos-core\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-07-03 16:55+0200\n"
"PO-Revision-Date: 2016-06-10 10:53+0000\n"
"Last-Translator: Aimeos <aimeos@aimeos.org>\n"
"Language-Team: Slovak (http://www.transifex.com/aimeos/aimeos-core/language/sk/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sk\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

msgid "Admin interface jobs"
msgstr ""

msgid ""
"Authorized payments of orders will be captured after dispatching or after a "
"configurable amount of time"
msgstr ""

msgid "Batch update of payment/delivery status"
msgstr ""

msgid "Cache cleanup"
msgstr ""

msgid "Capture authorized payments"
msgstr ""

msgid "Catalog index optimization"
msgstr ""

msgid "Catalog index rebuild"
msgstr ""

#, php-format
msgid "Class \"%1$s\" does not implement interface \"%2$s\""
msgstr "Trieda  \"%1$s\" neimplementuje rozhranie  \"%2$s\""

#, php-format
msgid "Class \"%1$s\" not available"
msgstr "Trieda  \"%1$s\" nie je dostupná"

#, php-format
msgid "Class \"%1$s\" not found"
msgstr "Trieda \"%1$s\" nenájdená"

#, php-format
msgid "Controller path is empty"
msgstr "Cesta ovládača je prázdná"

msgid "Creates a product site map for search engines"
msgstr ""

msgid "Creates bought together product suggestions"
msgstr ""

msgid "Customer account e-mails"
msgstr ""

msgid "Decreases the counts of successfully redeemed coupons"
msgstr ""

msgid "Decreases the stock levels of products in completed orders"
msgstr ""

msgid ""
"Deletes unfinished orders an makes their products and coupon codes available"
" again"
msgstr ""

msgid "Deletes unpaid orders to keep the database clean"
msgstr ""

msgid "Deprecated: Use \"Remove unfinised orders\""
msgstr ""

msgid "Deprecated: Use order/service/delivery"
msgstr ""

msgid "Executes payment or delivery service providers that uses batch updates"
msgstr ""

msgid ""
"Executes the jobs created by the admin interface, e.g. the text exports"
msgstr ""

msgid "Exports all available products"
msgstr ""

msgid "Imports new and updates existing products from CSV files"
msgstr ""

#, php-format
msgid "Invalid JSON encode message: %1$s"
msgstr ""

#, php-format
msgid "Invalid characters in class name \"%1$s\""
msgstr "Neplatné znaky v triede  \"%1$s\""

#, php-format
msgid "Invalid characters in job name \"%1$s\""
msgstr ""

#, php-format
msgid "Invalid controller \"%1$s\" in \"%2$s\""
msgstr "Invalid controller \"%1$s\" in \"%2$s\""

#, php-format
msgid "Invalid domain \"%1$s\""
msgstr "Neplatná doména  \"%1$s\""

#, php-format
msgid "Invalid factory \"%1$s\""
msgstr "Neplatná továreň \"%1$s\""

#, php-format
msgid "Invalid job method \"%1$s\""
msgstr ""

#, php-format
msgid "Invalid product lines in \"%1$s\": %2$d/%3$d"
msgstr ""

#, php-format
msgid "List type for domain \"%1$s\" and code \"%2$s\" not found"
msgstr "Typ záznamu nenájdený pre doménu \"%1$s\" a kód \"%2$s\""

msgid "Log cleanup"
msgstr ""

#, php-format
msgid "Method \"%1$s\" not available"
msgstr ""

#, php-format
msgid "No \"product.code\" column in CSV mapping found"
msgstr ""

#, php-format
msgid "No type item for \"%1$s/%2$s\" in \"%3$s\" found"
msgstr "No type item for \"%1$s/%2$s\" in \"%3$s\" found"

#, php-format
msgid "No warehouse \"%1$s\" found"
msgstr ""

msgid "Optimizes the catalog index for searching products"
msgstr ""

msgid "Order delivery related e-mails"
msgstr ""

msgid "Order payment related e-mails"
msgstr ""

msgid "Process order delivery services"
msgstr ""

msgid "Product export"
msgstr ""

msgid "Product import CSV"
msgstr ""

msgid "Product notification e-mails"
msgstr ""

msgid "Product site map"
msgstr ""

msgid "Products bought together"
msgstr ""

msgid "Rebuilds the catalog index for searching products"
msgstr ""

msgid "Removes the expired entries from the cache"
msgstr ""

msgid ""
"Removes the old log entries from the database and archives them (optional)"
msgstr ""

msgid "Removes unfinished orders"
msgstr ""

msgid "Removes unpaid orders"
msgstr ""

#, php-format
msgid "Required mapping key \"%1$s\" is missing or contains no array"
msgstr ""

msgid "Sends e-mails for new customer accounts"
msgstr ""

msgid "Sends e-mails for watched products"
msgstr ""

msgid "Sends order confirmation or payment status update e-mails"
msgstr ""

msgid "Sends order delivery status update e-mails"
msgstr ""

msgid "Sends paid orders to the ERP system or logistic partner"
msgstr ""

#, php-format
msgid "Started product import from \"%1$s\" (%2$s)"
msgstr ""

#, php-format
msgid "Template \"%1$s\" not available"
msgstr "Šablóna \"%1$s\" nie je dostupná"

#, php-format
msgid "Unable to call factory method \"%1$s\""
msgstr ""

#, php-format
msgid "Unable to call method \"%1$s\""
msgstr "Nepodarilo sa zavolať metódu \"%1$s\""

#, php-format
msgid "Unable to import product with code \"%1$s\": %2$s"
msgstr ""

#, php-format
msgid "Unable to move imported file"
msgstr ""

#, php-format
msgid "aimeos-products-%d.xml"
msgstr ""

#, php-format
msgid "aimeos-sitemap-%d.xml"
msgstr ""

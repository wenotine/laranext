<?php

return array(

	'routes' => array(
		 'login' => array('middleware' => ['web']),
		 'admin' => array('middleware' => ['web', 'auth']),
		 'account' => array('prefix' => '{locale}/{currency}', 'middleware' => ['web', 'auth']),
		 'default' => array('prefix' => '{locale}/{currency}', 'middleware' => ['web']),
		 'confirm' => array('prefix' => '{locale}/{currency}', 'middleware' => ['web']),
		 'update' => array('prefix' => '{locale}/{currency}'),
//         'account' => array( 'prefix' => '{locale}/{currency}', ... ),
//         'default' => array( 'prefix' => '{locale}/{currency}', ... ),
//         'confirm' => array( 'prefix' => '{locale}/{currency}', ... ),
//         'update' => array( 'prefix' => '{locale}/{currency}', ... ),
	),



    'page' => array(
        'account-index' => array( 'locale/select','account/history','account/favorite','account/watch','basket/mini','catalog/session' ),
        'basket-index' => array( 'locale/select','basket/standard','basket/related' ),
        'catalog-detail' => array( 'locale/select','basket/mini','catalog/stage','catalog/detail','catalog/session' ),
        'catalog-lists' => array( 'locale/select','basket/mini','catalog/filter','catalog/stage','catalog/lists' ),
        // ...
    ),
//	'page' => array(
//		// 'account-index' => array( 'account/history','account/favorite','account/watch','basket/mini','catalog/session' ),
//		// 'basket-index' => array( 'basket/standard','basket/related' ),
//		// 'catalog-count' => array( 'catalog/count' ),
//		// 'catalog-detail' => array( 'basket/mini','catalog/stage','catalog/detail','catalog/session' ),
//		// 'catalog-list' => array( 'basket/mini','catalog/filter','catalog/stage','catalog/lists' ),
//		// 'catalog-stock' => array( 'catalog/stock' ),
//		// 'catalog-suggest' => array( 'catalog/suggest' ),
//		// 'checkout-confirm' => array( 'checkout/confirm' ),
//		// 'checkout-index' => array( 'checkout/standard' ),
//		// 'checkout-update' => array( 'checkout/update'),
//	),

	'resource' => array(
		'db' => array(
			// 'adapter' => env('DB_CONNECTION', 'mysql'),
			// 'host' => env('DB_HOST', 'localhost'),
			// 'port' => env('DB_PORT', ''),
			// 'socket' => '',
			// 'database' => env('DB_DATABASE', 'laravel'),
			// 'username' => env('DB_USERNAME', 'root'),
			// 'password' => env('DB_PASSWORD', ''),
		),
	),

	'client' => array(
		'html' => array(
			'common' => array(
				'content' => array(
					// 'baseurl' => '/',
				),
				'template' => array(
					// 'baseurl' => 'packages/aimeos/shop/elegance',
				),
			),
		),
	),

	'controller' => array(
	),

	'i18n' => array(
	),
    'madmin' => array(
        'cache' => array(
            'manager' => array(
                'name' => 'None'
            ),
        ),
    ),

	'mshop' => array(
	),

);

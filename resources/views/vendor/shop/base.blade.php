@extends('app')

@section('aimeos_styles')
    <link rel="stylesheet" href="{{ asset('packages/aimeos/shop/themes/classic/common.css') }}" />
    <link rel="stylesheet" href="{{ asset('packages/aimeos/shop/themes/classic/aimeos.css') }}" />
@stop

@section('aimeos_scripts')
    <script type="text/javascript" src="{{ asset('packages/aimeos/shop/themes/jquery-ui.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('packages/aimeos/shop/themes/aimeos.js') }}"></script>
    <script type="text/javascript" src="{{ asset('packages/aimeos/shop/themes/classic/aimeos.js') }}"></script>
@stop

